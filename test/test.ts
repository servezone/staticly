import { expect, tap } from '@pushrocks/tapbundle';
import * as staticly from '../ts/index';

process.env.CLI_CALL = 'true';

tap.test('should start the instance', async () => {
  await staticly.runCli();
});

tap.test('should stop the instance', async () => {
  await staticly.stop();
});

tap.start();
