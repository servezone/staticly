import * as plugins from './staticly.plugins';
import * as paths from './staticly.paths';

export class WorkspaceServer {
  public readyDeferred = plugins.smartpromise.defer();
  public projectInfo = new plugins.projectinfo.ProjectinfoNpm(paths.packageDir);

  public options: {
    port: string;
    serveDir: string;
    baseLevel: number;
    serveIndexHtmlDefault: boolean;
    angular?: boolean;
  };

  public smartexpressServer: plugins.smartexpress.Server;

  public async init(argvArg: any) {
    console.log(`starting staticly in version ${this.projectInfo.version}`);

    // lets define some standard options
    this.options = {
      port: '3000',
      baseLevel: 0,
      serveIndexHtmlDefault: true,
      serveDir: process.cwd(),
      angular: false
    };

    // lets setup the env variables
    let requiredEnvVars: string[] = [];
    const availableEnvVars: string[] = [];
    const missingEnvVars: string[] = [];

    if (argvArg.reqEnv as string) {
      requiredEnvVars = requiredEnvVars.concat(argvArg.reqEnv.split(','));
    }

    if (requiredEnvVars.length > 0) {
      console.log(`${requiredEnvVars.length} required env vars specified. Checking for them now`);
      for (const envVar of requiredEnvVars) {
        process.env[envVar] ? availableEnvVars.push(envVar) : missingEnvVars.push(envVar);
      }
      if (missingEnvVars.length > 0) {
        console.log(`some environment variables are missing: ${missingEnvVars}`);
        process.exit(1);
      } else {
        console.log(`Great! No missing environment variables!`);
      }
    } else {
      console.log(`no required env vars specified, so we will just continue.`);
    }

    // lets determine the port
    argvArg.port ? (this.options.port = argvArg.port) : null;
    process.env.PORT ? (this.options.port = process.env.PORT) : null;

    // lets determine the serveDir
    argvArg.serveDir
      ? (this.options.serveDir = plugins.path.join(process.cwd(), argvArg.serveDir))
      : null;
    process.env.SERVE_DIR
      ? (this.options.serveDir = plugins.path.join(process.env.SERVE_DIR))
      : null;

    // lets determine the baseLevel
    argvArg.baseLevel ? (this.options.baseLevel = parseInt(argvArg.baseLevel, 10)) : null;
    process.env.BASE_LEVEL ? (this.options.baseLevel = parseInt(process.env.BASE_LEVEL, 10)) : null;

    console.log(`The server is trying to start on port ${this.options.port}`);

    // lets setup the server
    this.smartexpressServer = new plugins.smartexpress.Server({
      port: this.options.port,
      cors: false,
      forceSsl: false,
    });

    // lets create an serveHash
    const serveHash = await plugins.smartfile.fs.fileTreeToHash(this.options.serveDir, '**/*');
    const envVariables: any = {};
    for (const envVar of availableEnvVars) {
      envVariables[envVar] = process.env[envVar];
    }
    envVariables.serveHash = serveHash;

    this.smartexpressServer.addRoute(
      '/*',
      new plugins.smartexpress.HandlerStatic(this.options.serveDir, {
        requestModifier: async (reqArg) => {
          const pathArray = reqArg.path.split('/');
          let basePath = '';
          let folderPath = '';
          let i = 0;

          while (i <= this.options.baseLevel) {
            basePath += pathArray[i];
            basePath += '/';
            i++;
          }
          

          while (i < pathArray.length) {
            folderPath += '/';
            folderPath += pathArray[i];
            i++;
          }

          return {
            headers: reqArg.headers,
            path: folderPath,
            body: reqArg.body,
            travelData: basePath
          };
        },
        responseModifier: async (responseArg) => {
          let fileString = responseArg.responseContent;
          if (plugins.path.parse(responseArg.path).ext === '.html') {
            const fileStringArray = fileString.split('<head>');
            if (fileStringArray.length === 2) {
              fileStringArray[0] = `${fileStringArray[0]}<head>
                <script>
                  window.envVariables = ${JSON.stringify(envVariables)}
                </script>
            `;
              fileString = fileStringArray.join('');
            } else {
              console.log('Could not insert smartserve script');
            }
          }
          const headers = responseArg.headers;
          headers.appHash = serveHash;
          return {
            headers,
            path: responseArg.path,
            responseContent: fileString,
          };
        },
        serveIndexHtmlDefault: true,
      })
    );

    await this.smartexpressServer.start();
    this.readyDeferred.resolve();
  }

  public async stop() {
    await this.readyDeferred.promise;
    await this.smartexpressServer.stop();
  }
}
