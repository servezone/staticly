import * as plugins from './staticly.plugins';
import * as paths from './staticly.paths';

import { Staticly } from './staticly.classes.workspaceserver';

let staticlyInstance: Staticly;

export const runCli = async (argvTest?: any) => {
  const projectinfo = new plugins.projectinfo.ProjectinfoNpm(paths.packageDir);
  console.log(`staticly v${projectinfo.version}`);
  const smartcli = new plugins.smartcli.Smartcli();

  smartcli.standardTask().subscribe(async (argvArg) => {
    staticlyInstance = new Staticly();
    staticlyInstance.init(argvArg);
  });

  smartcli.startParse();
};

export const stop = async () => {
  await plugins.smartdelay.delayFor(1000);
  await staticlyInstance.stop();
};
