# @servezone/staticly
a server serving static builds in docker contexts

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@servezone/staticly)
* [gitlab.com (source)](https://gitlab.com/servezone/staticly)
* [github.com (source mirror)](https://github.com/servezone/staticly)
* [docs (typedoc)](https://servezone.gitlab.io/staticly/)

## Status for master
[![pipeline status](https://gitlab.com/servezone/staticly/badges/master/pipeline.svg)](https://gitlab.com/servezone/staticly/commits/master)
[![coverage report](https://gitlab.com/servezone/staticly/badges/master/coverage.svg)](https://gitlab.com/servezone/staticly/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@servezone/staticly.svg)](https://www.npmjs.com/package/@servezone/staticly)
[![Known Vulnerabilities](https://snyk.io/test/npm/@servezone/staticly/badge.svg)](https://snyk.io/test/npm/@servezone/staticly)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

Use TypeScript for best in class intellisense

## Contribution

We are always happy for code contributions. If you are not the code contributing type that is ok. Still, maintaining Open Source repositories takes considerable time and thought. If you like the quality of what we do and our modules are useful to you we would appreciate a little monthly contribution: You can [contribute one time](https://lossless.link/contribute-onetime) or [contribute monthly](https://lossless.link/contribute). :)

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
